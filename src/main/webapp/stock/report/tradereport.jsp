<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./tradereport.js"></script>
	
</head>

<body>
	<div id="toolbar">
	   <a class="easyui-linkbutton" iconCls="icon-add" onclick="click_add()">新增</a>
       <a class="easyui-linkbutton" iconCls="icon-remove" onclick="javascript:$('#dg').edatagrid('destroyRow')">删除</a>    
	</div>
	<table id="dg"  class="easyui-datagrid"
            data-options="rownumbers:true,singleSelect:true,pagination:true,method:'post',idField:'billId',
            	sortOrder:'desc', sortName:'billDate', editing:false,fit:true,singleSelect:true,
            	title:'交易管理', toolbar:'#toolbar',
            	url : plat.fullUrl('/report/tradereport/pagequery.do')
            ">
     	<thead>
            <tr>
           	 	<th data-options="field:'dateFrom',width:80,order:'desc'" sortable="true">开始时间</th>
           	 	<th data-options="field:'dateTo',width:80,order:'desc'">结束时间</th>
                <th data-options="field:'tradeTimes',width:80">交易次数</th>
                <th data-options="field:'totalMoney',width:100,align:'right'">交易金额</th>
                <th data-options="field:'totalFee',width:100,align:'right'">交易费用</th>
            </tr>
        </thead>
    </table>
</body>
</html>
