<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./assertstatereport.js"></script>
	<script src="../../core/highcharts/highstock.js"></script>
	
</head>

<body>
	<div  id="summary">
		<form id="ff" method="post" ajax="true">
			<table cellpadding="5">
				<tr>
					<td>从:</td>
					<td><input name='beginDay' class="easyui-datebox"/></td>
					<td>到:</td>
					<td><input name='endDay' class="easyui-datebox"/></td>
					<td>周期:</td>
					<td><input id="period" name="period" class="easyui-combobox" 
    					data-options="value:'week',valueField:'code',textField:'name',data:plat.FlexUtil.getValue('SD_PERIOD1')"/>
					</td>
					<td><a class="easyui-linkbutton" iconCls="icon-search" onclick="getReport()">查询</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="container">
		查询中.......
	</div>
</body>
</html>
