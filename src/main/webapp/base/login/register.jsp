<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>登录-股市日记</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="登录页面">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script>
		var _win=plat.WinUtil.top();
		if(_win!=window){
			_win.location.href=window.location.href; 
			_win.location.reload; 
		}
	</script>
  </head>
  
  <body>
	<div class="easyui-panel" title="用户注册" style="width:400px;padding:10px 60px 20px 60px">
		<form id="form1" name="form1"  class="easyui-form" method="post" data-options="novalidate:true,ajax:false">
			<div style="margin-bottom:10px">
				<input name="userName" class="easyui-textbox" required="true" style="width:100%;height:40px;padding:12px"
					data-options="prompt:'Username',iconCls:'icon-man',iconWidth:38">
			</div>
			<div style="margin-bottom:20px">
				<input name="userPwd" class="easyui-textbox" type="password" required="true" style="width:100%;height:40px;padding:12px"
					data-options="prompt:'Password',iconCls:'icon-lock',iconWidth:38">
			</div>
			<div style="margin-bottom:20px">
				<input type="checkbox" name="remPwd" checked="checked"><span>记住密码</span>
			</div>
			<div>
				<a  class="easyui-linkbutton" onClick="doLogin()" data-options="iconCls:'icon-ok'" style="padding:5px 0px;width:45%;">
				 	<span style="font-size:14px;">Login</span>
				</a>
				<a  class="easyui-linkbutton" data-options="iconCls:'icon-add'" style="padding:5px 0px;width:45%"
					href="${pageContext.request.contextPath}/base/login/register.jsp">
				 	<span style="font-size:14px;">Register</span>
				</a>
			</div>
		</form>
	</div>
</body>
</html>

<script type="text/javascript">

	$(function(){
			var a=$.cookie("userName");
			var b=$.cookie("userPwd");
			$("#form1").form("load",{userName:a,userPwd:b});
		}	
	);

	function doLogin() {
		var a=$("#form1").form('enableValidation').form('validate');
		if(a){
			document.form1.action="${pageContext.request.contextPath}/base/userlogin/login.do";
			document.form1.submit();
		}
	}
</script>
